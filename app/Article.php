<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
