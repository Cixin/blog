<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryTest extends TestCase
{
    /** @test */
    public function a_guest_cannot_see_the_category_creation_form()
    {
        $this->get('/admin/category/create')
            ->assertStatus(404);
    }

    /** @test */
    public function an_user_cannot_see_the_category_creation_form()
    {
        $user = factory('App\User')->create();

        $this->actingAs($user)
            ->get('/admin/category/create')
            ->assertStatus(404);
    }

    /** @test */
    public function an_administrator_can_see_the_category_creation_form()
    {
        $user = factory('App\User')->create(['administrator' => true]);

        $this->actingAs($user)
            ->get('/admin/category/create')
            ->assertStatus(200);
    }

    /** @test */
    public function a_guest_cannot_create_a_category()
    {
        $this->post('/admin/category', [])
            ->assertStatus(404);
    }

    /** @test */
    public function an_user_cannot_create_a_category()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->post('/admin/category', [])
            ->assertStatus(404);
    }

    /** @test */
    public function an_administrator_can_create_a_category()
    {
        $user = factory(User::class)->create(['administrator' => true]);

        $this->actingAs($user)
            ->post('/admin/category', [
                'name' => 'Category 1'
            ])->assertStatus(200);

        $this->assertDatabaseHas('categories', [
            'name' => 'Category 1'
        ]);
    }
}
